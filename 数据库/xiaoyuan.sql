-- MySQL dump 10.13  Distrib 5.5.57, for Linux (x86_64)
--
-- Host: localhost    Database: xiaoyuan
-- ------------------------------------------------------
-- Server version	5.5.57-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addata`
--

DROP TABLE IF EXISTS `addata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ad_type` varchar(234) NOT NULL,
  `ad_content` mediumtext NOT NULL,
  `ad_list` varchar(25) NOT NULL,
  `pv` varchar(30) NOT NULL,
  `endtime` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pv` (`pv`),
  KEY `endtime` (`endtime`),
  KEY `ad_list` (`ad_list`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addata`
--

LOCK TABLES `addata` WRITE;
/*!40000 ALTER TABLE `addata` DISABLE KEYS */;
/*!40000 ALTER TABLE `addata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admindata`
--

DROP TABLE IF EXISTS `admindata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admindata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `q` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`),
  KEY `password` (`password`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admindata`
--

LOCK TABLES `admindata` WRITE;
/*!40000 ALTER TABLE `admindata` DISABLE KEYS */;
INSERT INTO `admindata` VALUES (8,'admin','asdasd','最高权限'),(10,'admin123','admin123','最高权限');
/*!40000 ALTER TABLE `admindata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `article`
--

DROP TABLE IF EXISTS `article`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `article` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `top` varchar(1) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  `pic` varchar(999) NOT NULL,
  `type` varchar(25) NOT NULL,
  `pv` varchar(20) NOT NULL,
  `pv_max` varchar(30) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `day` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `title` (`title`),
  KEY `pv` (`pv`),
  KEY `day` (`day`)
) ENGINE=MyISAM AUTO_INCREMENT=347 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `article`
--

LOCK TABLES `article` WRITE;
/*!40000 ALTER TABLE `article` DISABLE KEYS */;
INSERT INTO `article` VALUES (346,'0','宝塔linux管理助手详细安装流程','<div class=\"content\" style=\"margin:10px 0px 30px;padding:0px;width:730px;color:#474747;text-transform:none;line-height:24px;text-indent:0px;letter-spacing:normal;font-family:微软雅黑;font-size:14px;font-style:normal;font-weight:normal;word-spacing:0px;float:left;white-space:normal;box-sizing:inherit !important;orphans:2;widows:2;font-variant-ligatures:normal;font-variant-caps:normal;-webkit-text-stroke-width:0px;\">\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		一.安装指令：&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n两行一起复制，在ssh里粘贴回车即可.&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n若您在安装过程中SSH中断，重新连接后，无需重新安装，输入 screen -r bt 即可恢复之前的安装过程&nbsp;&nbsp;\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<code style=\"border-width:1px 1px 1px 3px;border-style:solid;border-color:#DDDDDD #DDDDDD #DDDDDD #999999;margin:0px 0px 15px;padding:15px;line-height:60px;font-size:12px;box-sizing:inherit !important;background-color:#EEEEEE;\">yum -y install screen wget &amp;&amp; echo -e “defencoding utf-8\\nencoding utf-8 utf-8” &gt;&gt; /etc/screenrc &amp;&amp; screen -S bt</code><br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n<code style=\"border-width:1px 1px 1px 3px;border-style:solid;border-color:#DDDDDD #DDDDDD #DDDDDD #999999;margin:0px 0px 15px;padding:15px;line-height:60px;font-size:12px;box-sizing:inherit !important;background-color:#EEEEEE;\">wget -O install.sh<span class=\"Apple-converted-space\">&nbsp;</span><a style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;color:#333333;text-decoration:none;box-sizing:inherit !important;-webkit-tap-highlight-color:rgba(0, 0, 0, 0);\" href=\"http://125.88.182.172:5880/src/install.sh\">http://125.88.182.172:5880/src/install.sh</a><span class=\"Apple-converted-space\">&nbsp;</span>&amp;&amp; sh install.sh</code>\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		二.安装过程：&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n1.选择下载点，同时会提醒是否挂载www目录，因为程序默认安装在www目录下，如果有空的数据盘可输入y进入自动挂载，否则输入n。&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568224774.png\" src=\"http://image.zun.gd:3111/20160920/14743568224774.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		2.选择PHP版本，选项7为同时安装1-6 六个PHP版本，如果不需要那么多版本，可输其他选项以减少安装时所花费的时间，同时后期如需更多版本，可通过命令：sh /www/server/install.sh add 添加更多版本。&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568462999.png\" src=\"http://image.zun.gd:3111/20160920/14743568462999.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		3.选择MYSQL版本，并设置ROOT密码&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568493099.png\" src=\"http://image.zun.gd:3111/20160920/14743568493099.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568472103.png\" src=\"http://image.zun.gd:3111/20160920/14743568472103.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		4.核对信息，以下显示的耗时仅作参考，实际情况为主机性能越高安装时间越短，输入y开始安装。&nbsp;\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568507371.png\" src=\"http://image.zun.gd:3111/20160920/14743568507371.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n5.安装完成，阿里云以及腾讯云等服务器因第一张网卡的IP为内网IP所以URL会显示为内网IP，阿里云用户安装完后使用公网IP+888端口即可在外网访问。&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568511226.png\" src=\"http://image.zun.gd:3111/20160920/14743568511226.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n三.初始化：&nbsp;<br style=\"margin:0px;padding:0px;box-sizing:inherit !important;\" />\r\n访问：http://公网IP地址:888&nbsp;&nbsp; 进行初始化，设置管理员邮箱以及账号密码即可，其他信息系统会自动获取。&nbsp;\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		<img style=\"margin:0px;padding:0px;border:0px currentColor;border-image:none;max-width:100%;box-sizing:inherit !important;\" alt=\"14743568424260.png\" src=\"http://image.zun.gd:3111/20160920/14743568424260.png\" />\r\n	</p>\r\n	<p style=\"margin:0px 0px 6px;padding:0px;box-sizing:inherit !important;\">\r\n		以上操作完成后，即可开始使用BT 服务器管理助手了。&nbsp;\r\n	</p>\r\n</div>\r\n<div class=\"mip-box-footer item-up-down-page \" style=\"margin:0px;padding:0px;color:#000000;text-transform:none;text-indent:0px;letter-spacing:normal;overflow:hidden;clear:both;font-family:Helvetica, Arial, &quot;Hiragino Sans GB&quot;, &quot;Microsoft Yahei&quot;, 微软雅黑, STHeiti, 华文细黑, sans-serif;font-size:12px;font-style:normal;font-weight:normal;word-spacing:0px;border-top-color:#ECECEC;border-top-width:1px;border-top-style:solid;white-space:normal;box-sizing:inherit !important;orphans:2;widows:2;font-variant-ligatures:normal;font-variant-caps:normal;-webkit-text-stroke-width:0px;\">\r\n	<ul class=\"clearfix list-unstyled\" style=\"list-style:none;margin:0px;padding:0px;box-sizing:inherit !important;\">\r\n		<li class=\"item-up-page\" style=\"margin:5px 0px;padding:20px 0px 0px;width:auto;line-height:15px;float:left;box-sizing:inherit !important;\">\r\n			<br class=\"Apple-interchange-newline\" />\r\n		</li>\r\n	</ul>\r\n</div>','/editor/attached/image/20171014/20171014131505_66150.jpg','42','0','-1',0.05,'2017-10-14');
/*!40000 ALTER TABLE `article` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `koudata`
--

DROP TABLE IF EXISTS `koudata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `koudata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` varchar(30) NOT NULL,
  `aid` int(30) NOT NULL,
  `title` varchar(255) NOT NULL,
  `long` varchar(999) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `day` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `koudata`
--

LOCK TABLES `koudata` WRITE;
/*!40000 ALTER TABLE `koudata` DISABLE KEYS */;
/*!40000 ALTER TABLE `koudata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `newsdata`
--

DROP TABLE IF EXISTS `newsdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `newsdata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `newsdata`
--

LOCK TABLES `newsdata` WRITE;
/*!40000 ALTER TABLE `newsdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `newsdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refererdata`
--

DROP TABLE IF EXISTS `refererdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refererdata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `aid` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `long` mediumtext NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `width` varchar(10) NOT NULL,
  `height` varchar(10) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `day` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=2953 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refererdata`
--

LOCK TABLES `refererdata` WRITE;
/*!40000 ALTER TABLE `refererdata` DISABLE KEYS */;
INSERT INTO `refererdata` VALUES (2952,1534,0,'ID：1534用户登录','',0.00,'','','114.234.228.161','2017-10-14','1507958201');
/*!40000 ALTER TABLE `refererdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sharedata`
--

DROP TABLE IF EXISTS `sharedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sharedata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `aid` int(10) NOT NULL,
  `title` varchar(255) NOT NULL,
  `long` mediumtext NOT NULL,
  `ip` varchar(30) NOT NULL,
  `day` varchar(30) NOT NULL,
  `time` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM AUTO_INCREMENT=200 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sharedata`
--

LOCK TABLES `sharedata` WRITE;
/*!40000 ALTER TABLE `sharedata` DISABLE KEYS */;
/*!40000 ALTER TABLE `sharedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tongjidata`
--

DROP TABLE IF EXISTS `tongjidata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tongjidata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ip` varchar(20) DEFAULT NULL,
  `day` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `day` (`day`),
  KEY `day_2` (`day`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tongjidata`
--

LOCK TABLES `tongjidata` WRITE;
/*!40000 ALTER TABLE `tongjidata` DISABLE KEYS */;
/*!40000 ALTER TABLE `tongjidata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `txdata`
--

DROP TABLE IF EXISTS `txdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `txdata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` varchar(30) NOT NULL,
  `sx1` varchar(30) NOT NULL COMMENT '一级上线',
  `realname` varchar(55) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `alipay` varchar(255) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `state` varchar(1) NOT NULL COMMENT '0等待，1确认，2拒绝',
  `time` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `txdata`
--

LOCK TABLES `txdata` WRITE;
/*!40000 ALTER TABLE `txdata` DISABLE KEYS */;
/*!40000 ALTER TABLE `txdata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `typedata`
--

DROP TABLE IF EXISTS `typedata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `typedata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `type_pp` decimal(10,2) NOT NULL,
  `type_author` varchar(999) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `typedata`
--

LOCK TABLES `typedata` WRITE;
/*!40000 ALTER TABLE `typedata` DISABLE KEYS */;
INSERT INTO `typedata` VALUES (42,'教程',0.05,'');
/*!40000 ALTER TABLE `typedata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `userdata`
--

DROP TABLE IF EXISTS `userdata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tj_id` int(10) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `money` decimal(10,2) NOT NULL,
  `wx` varchar(255) NOT NULL,
  `realname` varchar(25) NOT NULL,
  `alipay` varchar(100) NOT NULL,
  `wgateid` varchar(255) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `kou` varchar(10) NOT NULL,
  `day` varchar(10) NOT NULL,
  `time` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `phone` (`phone`),
  KEY `pass` (`pass`),
  KEY `tj_id` (`tj_id`),
  KEY `money` (`money`),
  KEY `realname` (`realname`),
  KEY `alipay` (`alipay`),
  KEY `day` (`day`)
) ENGINE=MyISAM AUTO_INCREMENT=1535 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `userdata`
--

LOCK TABLES `userdata` WRITE;
/*!40000 ALTER TABLE `userdata` DISABLE KEYS */;
INSERT INTO `userdata` VALUES (1534,0,'13185807353','123456',1.00,'','','','','114.234.228.161','100','2017-10-14','1507958190');
/*!40000 ALTER TABLE `userdata` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-14 13:51:23
