<?php
session_start();
header("Cache-Control: no-cache, must-revalidate");		
/*
0：ok
32：非微信访问
31：非手机访问
3：来路错误
2：参数缺少
4：ip2段落重复
1：cookie存在
5：session验证码不存在
6：ip重复
*/
$post=$_REQUEST;
if(is_numeric($_SESSION['_share'])==false){
	exit('5');
}else{
	unset($_SESSION['_share']);
}
// if($_COOKIE['read']==1){
	// exit('1');
// }
//$post=file_get_contents("php://input");
require('conn.php');
require('functions.php');
ckk($config['UcApi']);
if(is_mobile()==false){
	exit('31');
}
if(is_weixin()==false){
	exit('32');
}
if($post['aid']=='' || $post['uid']=='' || $post['ip']=='' || $post['time']=='' || $post['title']==''){
	$mysql->__destruct();
	$mysql->close();
	exit('2');//缺少参数
}else{
	$aid=$post['aid'];
	$uid=$post['uid'];
	$time=$post['time'];
	$ip=$post['ip'];
	$day=date("Y-m-d",time());
}

//写入数据
$arr_share=array(
	'uid'=>$uid,
	'aid'=>$aid,
	'title'=>$post['title'],
	'long'=>$post['url'],
	'ip'=>$ip,
	'day'=>date("Y-m-d",time()),
	'time'=>$time,
);
$value=arr2s($arr_share);
$mysql->query("insert into `sharedata` {$value}");
//COOKIE 24+
//setcookie("read",'1', time()+3600*24);
$mysql->__destruct();
$mysql->close();
exit('0');
?>
